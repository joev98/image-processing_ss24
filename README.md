# Image Processing SS24

## Description
Solutions to assignments of the Image Processing course at FU Berlin Sommer Semester 2024. Topics:
- Images, Color Spaces
- Image Sensors, Interpolation
- Histograms, Image Enhancement
- Quality Estimation
- Spatial Filters
- Fourier Analysis, Convolution Theorem
- DFT, DCT, JPEG, Hadamard, STFT
- Haar-Wavelets, Lifting Scheme
- Image Pyramids, Blending
- Deconvolution

## Installation

Clone the repository:
```console
git clone https://git.imp.fu-berlin.de/joev98/image-processing_ss24.git
cd image-processing_ss24
python -m venv .venv
```

Activate the environment:
```console
source .venv/bin/activate      # MacOS / Linux
```
```console
source .venv/Scripts/activate  # MinGW
```
```console
.venv/Scripts/activate.bat     # Windows
```

Launch JupyterLab:
```console
pip install jupyterlab
jupyter lab
```

## Authors
- Vinzent Hannes Jörß
- Minh Tuan Nguyen
